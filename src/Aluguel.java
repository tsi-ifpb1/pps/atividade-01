
public class Aluguel {
	private Fita fita;
    private int diasAlugada;

    public Aluguel(Fita fita, int diasAlugada) {
        this.fita = fita;
        this.diasAlugada = diasAlugada;
    }

    public Fita getFita() {
        return fita;
    }

    public int getDiasAlugada() {
        return diasAlugada;
    }
    
    public double getTotalAluguel() {
    	
    	double valorCorrente = 0.0;
    	
    	switch (fita.getCódigoDePreço()) {
        case normal:
            valorCorrente += 2;
            if (diasAlugada > 2) {
                valorCorrente += (diasAlugada - 2) * 1.5;
            }
            break;
        case lancamento:
            valorCorrente += diasAlugada * 3;
            break;
        case infantil:
            valorCorrente += 1.5;
            if (diasAlugada > 3) {
                valorCorrente += (diasAlugada - 3) * 1.5;
            }
            break;
        } // switch
    	
    	return valorCorrente;
    }
    
    public boolean isGanhaBonus() {
    	
    	boolean result = false;
    	
    	if (fita.getCódigoDePreço() == Tipo.lancamento && diasAlugada > 1) {
        	result = true;
        }
    	return result;
    }
    
    public String getExtratoAluguel() {
    	
    	final String fimDeLinha = System.getProperty("line.separator");
    	String resultado = "";
    	
        resultado += "\t" + fita.getTítulo() + "\t"
                     + this.getTotalAluguel() + fimDeLinha;
        
        return resultado;
    }

}
